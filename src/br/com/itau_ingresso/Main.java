package br.com.itau_ingresso;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        Ingresso ingresso = new Ingresso(Double.parseDouble(JOptionPane.showInputDialog(null, "Digite o valor de ingresso: ", "Ingresso", JOptionPane.INFORMATION_MESSAGE)));
        VIP vip = new VIP(ingresso.getValorReais(), Double.parseDouble(JOptionPane.showInputDialog(null, "Digite o valor adicional: ", "Ingresso", JOptionPane.INFORMATION_MESSAGE)));

        Normal normal = new Normal(ingresso.getValorReais());

        JOptionPane.showMessageDialog(null, "Classe Normal: " +  normal.ingressoNormal() + "\n\"Classe VIP: " +
                vip.ingresso());
    }
}
