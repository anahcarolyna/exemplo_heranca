package br.com.itau_ingresso;

public class Ingresso {
    private double valorReais;

    public Ingresso(double valorReais) {
        this.valorReais = valorReais;
    }

    public double getValorReais() {
        return valorReais;
    }

    public void setValorReais(double valorReais) {
        this.valorReais = valorReais;
    }

    public double ingresso(){
        return valorReais;
    }
}
