package br.com.itau_ingresso;

public class VIP extends Ingresso{
    private double valorAdicional;

    public VIP(double valorReais, double valorAdicional) {
        super(valorReais);
        this.valorAdicional = valorAdicional;
    }

    public double getValorAdicional() {
        return valorAdicional;
    }

    @Override
    public double ingresso() {
        return super.ingresso() + this.getValorAdicional();
    }
}
