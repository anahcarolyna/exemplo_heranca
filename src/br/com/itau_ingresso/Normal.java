package br.com.itau_ingresso;

public class Normal extends Ingresso{

    public Normal(double valorReais) {
        super(valorReais);
    }

    public String ingressoNormal(){
        return "Ingresso Normal: " + ingresso();
    }
}
