package br.com.itau;

public class Pessoa {
    private String Nome;
    private String idade;
    private String nacionalidade;

    public Pessoa(String nome, String idade, String nacionalidade) {
        Nome = nome;
        this.idade = idade;
        this.nacionalidade = nacionalidade;
    }

    public String getNome() {
        return Nome;
    }

    public String getIdade() {
        return idade;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public void setIdade(String idade) {
        this.idade = idade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }
}


