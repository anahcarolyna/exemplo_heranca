package br.com.itau;

public class Main {
    public static void main(String[] args) {
        Pessoa pessoa = new Pessoa("Carol", "30", "Brasileira");
        Funcionario funcionario =  new Funcionario("Carlos", "36", "Brasileiro",2000.00,"Analista");

        System.out.println("Nome: " + funcionario.getNome() + "\nSalário:" +  funcionario.getSalario());
    }
}
